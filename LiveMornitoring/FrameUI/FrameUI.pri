FORMS += \
    $$PWD/FrameMain.ui \
    $$PWD/frmvideo.ui \
    $$PWD/FrameVideoPage.ui \
    $$PWD/FrameSetting.ui \
    $$PWD/settinguisub.ui \
    $$PWD/FrameState.ui

HEADERS += \
    $$PWD/FrameMain.h \
    $$PWD/frmvideo.h \
    $$PWD/FrameVideoPage.h \
    $$PWD/FrameSetting.h \
    $$PWD/settinguisub.h \
    $$PWD/FrameState.h

SOURCES += \
    $$PWD/FrameMain.cpp \
    $$PWD/frmvideo.cpp \
    $$PWD/FrameVideoPage.cpp \
    $$PWD/FrameSetting.cpp \
    $$PWD/settinguisub.cpp \
    $$PWD/FrameState.cpp
