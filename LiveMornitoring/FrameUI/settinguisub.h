#ifndef SETTINGUISUB_H
#define SETTINGUISUB_H

#include <QWidget>
#include "BasicFunction/myhelper.h"

namespace Ui {
class SettingUiSub;
}

class SettingUiSub : public QWidget
{
    Q_OBJECT

public:
    explicit SettingUiSub(QWidget *parent = 0);
    ~SettingUiSub();

private slots:

    void on_switchButtonFontSize_checkedChanged(bool checked);

    void on_switchButton_checkedChanged(bool checked);

    void on_btnFontSize_clicked();

    void on_btnFontType_clicked();

    void on_switchButtonStyle_checkedChanged(bool checked);

    void on_sliderBarColor_currentItemChanged(int index, const QString &item);

    void on_sliderBarIcon_currentItemChanged(int index, const QString &item);

    void on_btnStyle_clicked();

private:
    QString tempStyleName;
    int tempIconStyleName;
    Ui::SettingUiSub *ui;
    void iniConfig();
    void changeImage(int x, int y);
signals:
    void MainIconStyle(int);
};

#endif // SETTINGUISUB_H
