﻿#include "frmvideo.h"
#include "ui_frmvideo.h"
#include "BasicFunction/myhelper.h"
#include "VideoPlayerCore/VideoPlayer.h"
#include "VideoPlayerCore/playermultithreadentry.h"

QLabel *frmVideo::currentClickedLabel = 0;


frmVideo::frmVideo(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::frmVideo)
{
    ui->setupUi(this);
    this->initForm();
    QList<QLabel*> labels = ui->gboxVideo->findChildren<QLabel*>();
    QLabel* tl;
    foreach (tl, labels) {


        PlayerMultithreadEntry* pt;
        pt =  (new PlayerMultithreadEntry(tl,"http://pull-g.kktv8.com/livekktv/100987038.flv"));
        //X:\Code\Qt Project\Graduate\GAS
        //pt =  (new PlayerMultithreadEntry(tl,"http://server16.pkuml.org:8086/1"));

        pt->start();
    }

}

frmVideo::~frmVideo()
{
    delete ui;
}

void frmVideo::setDataForwardingCore(DataForwarding *dataForwardingCore)
{
    this->dataForwardingCore = dataForwardingCore;
}

void frmVideo::initForm()
{
    ui->treeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->treeWidget->setVisible(false);
    //connect(ui->treeWidget, SIGNAL(customContextMenuRequested(QPoint)),this, SLOT(slotShowTreeMenu()));
    connect(this,SIGNAL(clickedLabel(QLabel*)),this,SLOT(updateClickedLabel(QLabel*)));
    QList<QLabel *> labs = this->findChildren<QLabel *>();

    for (int i = 0; i < labs.count(); i++) {
        QLabel *lab = labs.at(i);
        //为每个视频通道设置右键菜单
        lab->setContextMenuPolicy(Qt::CustomContextMenu);
        //绑定 显示
        connect(lab, SIGNAL(customContextMenuRequested(QPoint)),this, SLOT(slotShowTreeMenu()));
        lab->setFrameShape(QFrame::Box);
        lab->setAlignment(Qt::AlignCenter);
        lab->setFocusPolicy(Qt::StrongFocus);
        lab->setText(QString("画面%1").arg(i + 1));
        lab->setObjectName(QString("labVideo%1").arg(i + 1));
    }

    //设置视频播放窗口的右键菜单信息
    treeMenu=new QMenu(ui->treeWidget);
    normalScreenAction = new QAction("退出全屏");//连接函数在FrameVideoPage.cpp
    fullScrn = new QAction("全屏播放");
    prtScrnAction = new QAction("截屏取证");
    pauseAction = new QAction("暂停播放");
    detailInfotreeMenu  = new    QAction("详情信息");
    labeling = new QAction("记录异常");
    treeMenu->addAction(normalScreenAction);
    treeMenu->addAction(fullScrn);
    treeMenu->addAction(prtScrnAction);
    treeMenu->addAction(pauseAction);
    treeMenu->addAction(detailInfotreeMenu);
    treeMenu->addAction(labeling);

    connect(fullScrn,SIGNAL(triggered(bool)),this,SLOT(SingleVideoFullScrn()));
    connect(normalScreenAction,SIGNAL(triggered(bool)),this,SLOT(scrnNormal()));

}

//视频窗口被右击后的槽函数，显示出菜单
void frmVideo::slotShowTreeMenu()
{
    //判断点击对象
    QLabel *lab = (QLabel *)sender();
    //将被点对象以信号的形式发送，为了之后如果用户点击单个视频全屏，知道是谁被点
    emit clickedLabel(lab);

    //显示菜单
    treeMenu->exec(QCursor::pos());
}

void frmVideo::updateClickedLabel(QLabel *lab)
{
    currentClickedLabel = lab;
}

void frmVideo::SingleVideoFullScrn()
{
    emit oneVideoWillFullScrn();//发送给FrameVideoPage

    if(!currentClickedLabel->isFullScreen()){//确定当前处于全屏幕状态
        App::ScrnState = 2;
        currentClickedLabel->setWindowFlags(Qt::Dialog);
        currentClickedLabel->showFullScreen();
        qDebug()<<"视频"<<currentClickedLabel<<"被全屏放大";
    }

}

void frmVideo::scrnNormal()
{
    qDebug()<<"单个视频 退出全屏";

    if(currentClickedLabel->isFullScreen()){//确定当前处于全屏幕状态
        App::ScrnState = 0;
        currentClickedLabel->setWindowFlags(Qt::SubWindow);
        currentClickedLabel->showNormal();
        qDebug()<<"视频"<<currentClickedLabel<<"被还原";
    }
}

//本函数用于多线程播放，由于界面显示操作必须在主线程中进行，所以多线程打开视频时，需要用
//主线程内的函数完成绘制任务
