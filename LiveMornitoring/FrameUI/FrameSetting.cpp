﻿#include "FrameSetting.h"
#include "ui_FrameSetting.h"
#include "BasicFunction/myhelper.h"

FrameSetting::FrameSetting(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FrameSetting)
{
    ui->setupUi(this);
    this->initForm();
    this->initConfig();
}

FrameSetting::~FrameSetting()
{
    delete ui;
}

void FrameSetting::initForm()
{

    ui->swhQuality->setChecked(true);
    ui->switchFrameRate->setChecked(true);
    ui->btnConfig->setCheckable(true);
    ui->btnConfig->setChecked(true);
    ui->btnConfig_2->setCheckable(true);
    ui->btnConfig_3->setCheckable(true);

    int btnWidth = 80;
    int btnHeight = 26;

    QList<SwitchButton *> btns1 = ui->gboxBase->findChildren<SwitchButton *>();

    foreach (SwitchButton *btn, btns1) {
        btn->setFixedSize(btnWidth, btnHeight);
    }


    //设置堆栈容器 0号是“系统”界面
    ui->stackedWidget->addWidget(new QWidget);
    SettingUiSub* s = new SettingUiSub;
    ui->stackedWidget->addWidget( s);

    //子界面SettingUiSub发出的主页面图标切换信号，经本对象再次顶层传递
    connect(s,SIGNAL(MainIconStyle(int)),this,SIGNAL(MainIconStyle(int)));

    QList<QPushButton *> btns = ui->widget_left->findChildren<QPushButton *>();
    foreach(QPushButton *btn, btns){
        connect(btn,SIGNAL(clicked(bool)),this,SLOT(buttonClick()));
    }

    ui->stackedWidget->setCurrentIndex(2);
}

void FrameSetting::initConfig()
{

}

void FrameSetting::saveConfig()
{

}

//处理左侧按钮点击
void FrameSetting::buttonClick()
{
    QPushButton* btn = (QPushButton*)sender();
    QString name = btn->text();
    QList<QPushButton* > btns= ui->widget_left->findChildren<QPushButton*>();
    QPushButton* temp;
    foreach(temp,btns){
        if(btn!=temp){
            temp->setChecked(false);
        }
    }

    if(name == "系统"){
        ui->stackedWidget->setCurrentIndex(0);
    }else if(name == "界面"){
        ui->stackedWidget->setCurrentIndex(2);
    }else if(name == "高级"){
        ui->stackedWidget->setCurrentIndex(1);
    }
}


//功能设置
void FrameSetting::on_autoBootSwh_checkedChanged(bool checked)
{
    App::autoBoot = checked;
}

void FrameSetting::on_autoLoginSwh_checkedChanged(bool checked)
{

}

void FrameSetting::on_autoFullscrnSwh_checkedChanged(bool checked)
{
    App::BootfullScreen = checked;
}

void FrameSetting::on_NoSpaceSwh_checkedChanged(bool checked)
{
    App::spaceAlert = checked;
}

void FrameSetting::on_abnormalSwh_checkedChanged(bool checked)
{
    App::abnormalAlert = checked;
}

void FrameSetting::on_signalAlertSwh_checkedChanged(bool checked)
{
    App::signalAlert = checked;
}

void FrameSetting::on_playerAlertSwh_checkedChanged(bool checked)
{
    App::playerAlert = checked;
}

void FrameSetting::on_buttomNameSwh_checkedChanged(bool checked)
{
    App::showTitleinLeftBottom = checked;
}

void FrameSetting::on_buttomTimeSwh_checkedChanged(bool checked)
{
    App::showCurrentTime = checked;
}

void FrameSetting::on_buttomCurUserSwh_checkedChanged(bool checked)
{
    App::showCurrentUser = checked;
}

void FrameSetting::on_switchButton_20_checkedChanged(bool checked)
{
    App::showSystemRunningTime = checked;
}

//保存到配置文件
void FrameSetting::on_pushButton_clicked()
{
    App::WriteConfig();
}

void FrameSetting::on_swhQuality_checkedChanged(bool checked)
{
    if(checked == true){

        myHelper::SetDisableSliderBarStyle(ui->sliderBar_Quality);
        ui->sliderBar_Quality->setEnabled(false);

        QImage q;
        q.load(":/banner/Dyna_HighQ.jpg");
        QPixmap pix = QPixmap::fromImage(q.scaled(ui->label_IMG->width(), ui->label_IMG->height(),
                                                  Qt::IgnoreAspectRatio,Qt::SmoothTransformation));
        ui->label_IMG->setPixmap(pix);

    }else{
        myHelper::SetEnableSliderBarStyle(ui->sliderBar_Quality);
        ui->sliderBar_Quality->setEnabled(true);
        QImage q;
        q.load(":/banner/HighQ.jpg");
        QPixmap pix = QPixmap::fromImage(q.scaled(ui->label_IMG->width(), ui->label_IMG->height(),
                                                  Qt::IgnoreAspectRatio,Qt::SmoothTransformation));
        ui->label_IMG->setPixmap(pix);

    }
}

void FrameSetting::on_switchFrameRate_checkedChanged(bool checked)
{
    if(checked == true){
        myHelper::SetDisableSliderBarStyle(ui->sliderBar_FrameRate);
        ui->sliderBar_FrameRate->setEnabled(false);
        App::intervalFrames = 25;
    }else{
        myHelper::SetEnableSliderBarStyle(ui->sliderBar_FrameRate);

        ui->sliderBar_FrameRate->setEnabled(true);

    }
}

void FrameSetting::on_sliderBar_Quality_currentItemChanged(int index, const QString &item)
{
    QImage q;
    if(index == 0){
        q.load(":/banner/LowQ.jpg");
        App::HQVideo = Qt::FastTransformation;
    }else if(index == 1){
        q.load(":/banner/MiddleQ.jpg");
        App::HQVideo = Qt::FastTransformation;

    }else if(index == 2){
        q.load(":/banner/HighQ.jpg");
        App::HQVideo = Qt::SmoothTransformation;
    }
    QPixmap pix = QPixmap::fromImage(q.scaled(ui->label_IMG->width(), ui->label_IMG->height(),
                                              Qt::IgnoreAspectRatio,Qt::SmoothTransformation));
    ui->label_IMG->setPixmap(pix);
}

void FrameSetting::on_sliderBar_FrameRate_currentItemChanged(int index, const QString &item)
{
    if(index == 0){
        App::intervalFrames = 25;
    }else if(index == 1){
        App::intervalFrames = 10;

    }else if(index == 2){
        App::intervalFrames = 3;
    }
}
