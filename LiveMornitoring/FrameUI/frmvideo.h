﻿#ifndef FRMVIDEO_H
#define FRMVIDEO_H

#include <QWidget>
#include <QLabel>
#include<Kernel/DataForwarding.h>

namespace Ui {
class frmVideo;
}

class frmVideo : public QWidget
{
    Q_OBJECT

public:
    explicit frmVideo(QWidget *parent = 0);
    ~frmVideo();
     void setDataForwardingCore(DataForwarding* dataForwardingCore);
     static QLabel *currentClickedLabel;

     QMenu *treeMenu;
     QAction * normalScreenAction;
     QAction * fullScrn;
     QAction * pauseAction;
     QAction * prtScrnAction;
     QAction * detailInfotreeMenu;
     QAction * labeling;
     Ui::frmVideo *ui;
private:
    //数据接收模块
     QMutex mutex;
    DataForwarding* dataForwardingCore;
private slots:
    void initForm();
    void slotShowTreeMenu();
    void updateClickedLabel(QLabel*lab);
    void SingleVideoFullScrn();
    void scrnNormal();//将单个全屏视频还原


signals:
    void clickedLabel(QLabel*);
    void oneVideoWillFullScrn();
};

#endif // FRMVIDEO_H
