﻿#include "settinguisub.h"
#include "ui_settinguisub.h"
#include "BasicFunction/myhelper.h"

SettingUiSub::SettingUiSub(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingUiSub)
{
    //ui->sliderBar->setCurrentItem(QString("a"));
    ui->setupUi(this);

    //初始化
    iniConfig();
    ui->sliderBarColor->setItems(QString("淡雅灰;经典银;深空蓝;泰泽黑"));
    ui->sliderBarIcon->setItems(QString("立体化;扁平化"));


}


void SettingUiSub::iniConfig()
{
    //保存到配置文件
    connect(ui->toolButton,&QToolButton::clicked,[=](){
        App::WriteConfig();
    });

    tempStyleName = App::StyleName;
    tempIconStyleName = App::MainUiIcon;

    if(App::StyleName == ":/qss/lightgray.css"){
        ui->sliderBarColor->setCurrentIndex(0);
    }
    else if(App::StyleName ==":/qss/darkgray.css")
    {
        ui->sliderBarColor->setCurrentIndex(1);
    }
    else if(App::StyleName == ":/qss/blue.css")
    {
        ui->sliderBarColor->setCurrentIndex(2);
    }
    else
    {
        ui->sliderBarColor->setCurrentIndex(3);
    }

    //初始化主页图标风格设置
    if(App::MainUiIcon == 1){

        ui->sliderBarIcon->setCurrentIndex(0);

    }else if(App::MainUiIcon == 2){

        ui->sliderBarIcon->setCurrentIndex(0);
    }

    //初始化锁定按钮状态
    //字号
    if(App::hasFontSizeLocked == true){
        ui->switchButtonFontSize->setChecked(true);
        on_switchButtonFontSize_checkedChanged(true);
    }
    else{
        ui->switchButtonFontSize->setChecked(false);
        on_switchButtonFontSize_checkedChanged(false);
    }

     //字体
    if(App::hasFontTypeLocked == true){
        ui->switchButton->setChecked(true);
        on_switchButton_checkedChanged(true);
    }
    else{
        ui->switchButton->setChecked(false);
        on_switchButton_checkedChanged(false);

    }

}

void SettingUiSub::changeImage(int x,int y)
{
//    ui->label->setVisible(false);
//    ui->roundWidget->setVisible(true);
//    QTimer *timer=new QTimer;
//    timer->setSingleShot(true);
//      QObject::connect(timer,&QTimer::timeout,[=]() {


//         ui->roundWidget->setVisible(false);
//         ui->label->setVisible(true);

         QImage q;
         bool f = q.load(QString(":/banner/%1,%2.png").arg(x).arg(y));
         qDebug()<<f;
         QPixmap pix = QPixmap::fromImage(q.scaled(ui->label->width(), ui->label->height(),
                                                   Qt::IgnoreAspectRatio,Qt::SmoothTransformation));
         ui->label->setPixmap(pix);

//     });
//     timer->start(100);


}


SettingUiSub::~SettingUiSub()
{
    delete ui;
}



void SettingUiSub::on_switchButtonFontSize_checkedChanged(bool checked)
{
    if(checked == true){//锁定状态
        ui->btnFontSize->setEnabled(false);
        ui->lineEdit->setEnabled(false);
    }else{
        ui->btnFontSize->setEnabled(true);
        ui->lineEdit->setEnabled(true);
    }
}

void SettingUiSub::on_switchButton_checkedChanged(bool checked)
{
    if(checked == true){//锁定状态
        ui->btnFontType->setEnabled(false);
        ui->fontComboBox->setEnabled(false);
    }else{
        ui->btnFontType->setEnabled(true);
        ui->fontComboBox->setEnabled(true);
    }
}

void SettingUiSub::on_switchButtonStyle_checkedChanged(bool checked)
{
    if(checked == true){
        ui->btnStyle->setEnabled(false);
        ui->sliderBarColor->setEnabled(false);
        ui->sliderBarIcon->setEnabled(false);
    }else{
        ui->btnStyle->setEnabled(true);
        ui->sliderBarColor->setEnabled(true);
        ui->sliderBarIcon->setEnabled(true);
    }
}


//TODO:添加保存配置文件
void SettingUiSub::on_btnFontSize_clicked()
{
    myHelper::showMessageBoxInfo("字体大小设置智能在重启窗体后生效",10);
}

void SettingUiSub::on_btnFontType_clicked()
{
    myHelper::showMessageBoxInfo("字体大小设置智能在重启窗体后生效",10);

}

//修改滑动栏的值
void SettingUiSub::on_sliderBarColor_currentItemChanged(int index, const QString &item)
{
    if(index == 0){
        tempStyleName = ":/qss/lightgray.css";
    }else if(index == 1){
        tempStyleName = ":/qss/darkgray.css";
    }else if(index == 2){
        tempStyleName = ":/qss/blue.css";
    }else{
        tempStyleName = ":/qss/brown.css";
    }

    changeImage(index+1,tempIconStyleName);
}

void SettingUiSub::on_sliderBarIcon_currentItemChanged(int index, const QString &item)
{
    if(index == 0){
        tempIconStyleName = 1;
    }else{
        tempIconStyleName = 2;
    }

    int  x;
    if(tempStyleName  == ":/qss/lightgray.css" ){

         x = 1;
    }else if(tempStyleName == ":/qss/darkgray.css"  ){

         x = 2;
    }else if(tempStyleName == ":/qss/blue.css" ){

         x = 3;
    }else{

         x = 4;
    }

    changeImage(x,tempIconStyleName);
}

//点击样式 “应用” 按钮
void SettingUiSub::on_btnStyle_clicked()
{
    App::StyleName = tempStyleName;
    App::MainUiIcon = tempIconStyleName;
    myHelper::setStyle(App::StyleName);
    emit MainIconStyle(App::MainUiIcon);
}
