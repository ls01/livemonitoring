#ifndef FRAMESTATE_H
#define FRAMESTATE_H

#include <QWidget>
#include"cpumemorylabel.h"

namespace Ui {
class FrameState;
}

class FrameState : public QWidget
{
    Q_OBJECT

public:
    explicit FrameState(QWidget *parent = 0);
    ~FrameState();

private:
    Ui::FrameState *ui;
    CpuMemoryLabel* cpu;
    void initForm();

private slots:
    void drawCpuAndMem(int cpuPercent, int memoryPercent, int memoryAll, int memoryUse, int memoryFree);


};

#endif // FRAMESTATE_H
