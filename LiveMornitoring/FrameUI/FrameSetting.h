﻿#ifndef FRMCONFIG_H
#define FRMCONFIG_H

#include <QWidget>
#include <QPushButton>
#include "settinguisub.h"

/**
* @file       filename
* @brief      This is a brief description.
* @details   设置界面
* @author     李晟
* @date       2017年4月7日
* @par History:
*   version: 李晟, 2017年4月7日\n
*/
namespace Ui {
class FrameSetting;
}

class FrameSetting : public QWidget
{
    Q_OBJECT

public:
    explicit FrameSetting(QWidget *parent = 0);
    ~FrameSetting();

private:
    Ui::FrameSetting *ui;

private slots:
    /**
     * @brief initForm
     */
    void initForm();
    /**
     * @brief initConfig
     */
    void initConfig();
    /**
     * @brief saveConfig
     */
    void saveConfig();
    void buttonClick();
    void on_autoBootSwh_checkedChanged(bool checked);

    void on_autoLoginSwh_checkedChanged(bool checked);

    void on_autoFullscrnSwh_checkedChanged(bool checked);

    void on_NoSpaceSwh_checkedChanged(bool checked);

    void on_abnormalSwh_checkedChanged(bool checked);

    void on_signalAlertSwh_checkedChanged(bool checked);

    void on_playerAlertSwh_checkedChanged(bool checked);

    void on_buttomNameSwh_checkedChanged(bool checked);

    void on_buttomTimeSwh_checkedChanged(bool checked);

    void on_buttomCurUserSwh_checkedChanged(bool checked);

    void on_switchButton_20_checkedChanged(bool checked);

    void on_pushButton_clicked();

    void on_swhQuality_checkedChanged(bool checked);

    void on_switchFrameRate_checkedChanged(bool checked);

    void on_sliderBar_Quality_currentItemChanged(int index, const QString &item);

    void on_sliderBar_FrameRate_currentItemChanged(int index, const QString &item);

signals:
    void MainIconStyle(int);
};

#endif // FRMCONFIG_H
