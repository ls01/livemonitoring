﻿#include "FrameVideoPage.h"
#include "ui_FrameVideoPage.h"
#include "BasicFunction/myhelper.h"
#include "frmvideo.h"

FrameVideoPage::FrameVideoPage(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::FrameVideoPage)
{
	ui->setupUi(this);
	this->initForm();
    //const geometry = new QRectF;

}

FrameVideoPage::~FrameVideoPage()
{
    delete ui;
}

void FrameVideoPage::setDataForwardingCore(DataForwarding *dataForwardingCore)
{
    this->dataForwardingCore = dataForwardingCore;
}

void FrameVideoPage::on_switchButton_checkedChanged(bool checked)
{
    if(checked == true){
        //geometry  = ui->stackedWidget->geometry();
        ui->stackedWidget->setFocusPolicy(Qt::NoFocus);
        this->setFocus();
        App::ScrnState = 1;
        ui->stackedWidget->setWindowFlags(Qt::Dialog);
        ui->stackedWidget->showFullScreen();
        qDebug()<<"全屏幕";
//        this->setGeometry(qApp->desktop()->availableGeometry());
//        ui->stackedWidget->layout()->setContentsMargins(0,0,0,0);
//        ui->widget_left_2->setVisible(false);
//        ui->widget_left->setVisible(false);

    }
}


void FrameVideoPage::scrnNormal()
{
    qDebug()<<"FrameVideoPage退出全屏";
    //if(ui->stackedWidget->isFullScreen()){//确定当前处于全屏幕状态
            App::ScrnState = 0;
            ui->stackedWidget->setGeometry(50,50,1000,500);

            ui->stackedWidget->setWindowFlags(Qt::SubWindow);
            ui->stackedWidget->showNormal();
            ui->switchButton->setChecked(false);

            timer->start(1000);
            //father->MinScreen();


    //}
}





void FrameVideoPage::keyPressEvent(QKeyEvent *event)
{
    qDebug()<<"键盘事件";
    if(ui->stackedWidget->isFullScreen()){//确定当前处于全屏幕状态
        if(event->key()==Qt::Key_Escape){

        }
    }
}

void FrameVideoPage::setFather(FrameMain *father)
{
    this->father = father;
}

void FrameVideoPage::initForm()
{
    timer = new QTimer(this);
    timer->setSingleShot(true);
    connect(timer,&QTimer::timeout,[=](){
        father->MinScreen();
    });

    frmVideo* f = new frmVideo;
    ui->stackedWidget->addWidget(f);
    ui->stackedWidget->setCurrentIndex(1);
	QString qss = "QTabBar::tab{min-width:47px;min-height:20px;}";
    connect(f->normalScreenAction, SIGNAL(triggered(bool)), this, SLOT(scrnNormal()));
    connect(f,SIGNAL(oneVideoWillFullScrn()),this,SLOT(scrnNormal()));
	if (App::UseStyle) {
		qss += "QTabWidget::pane{border:0px;}";
	}

}



//void FrameVideoPage::buttonClick()
//{
//	QPushButton *btn = (QPushButton *)sender();
//	QString name = btn->text();
//	buttonCheck(btn);

//	if (name == "地图监控") {
//		ui->stackedWidget->setCurrentIndex(0);
//	} else if (name == "视频监控") {
//		ui->stackedWidget->setCurrentIndex(1);
//	}
//}

//void FrameVideoPage::buttonCheck(QPushButton *btn)
//{
//	QList<QPushButton *> btns = ui->widget_left->findChildren<QPushButton *>();

//	foreach (QPushButton *b, btns) {
//		if (b == btn) {
//			b->setChecked(true);
//		} else {
//			b->setChecked(false);
//		}
//	}
//}

