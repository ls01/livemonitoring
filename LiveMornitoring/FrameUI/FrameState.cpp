#include "FrameState.h"
#include "ui_FrameState.h"

FrameState::FrameState(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FrameState)
{
    ui->setupUi(this);
    initForm();
}

FrameState::~FrameState()
{
    delete ui;
}

void FrameState::initForm()
{
    ui->progressBarRoundCpu->setColor(255,221,117);
    ui->progressBarRoundMem->setColor(117,207,255);
    ui->curveChartCpu->addData(10.9);
    cpu = new CpuMemoryLabel;
    cpu->start(2000);
    connect(cpu,SIGNAL(valueChanged(int , int , int , int , int)),this,SLOT(drawCpuAndMem(int,int,int,int,int)));
    connect(cpu,&CpuMemoryLabel::valueChanged,[=](int cpuPercent, int memoryPercent, int memoryAll, int memoryUse, int memoryFree){
        //qDebug()<<cpuPercent;
        if(cpuPercent>81)
            ui->curveChartCpu->setMaxValue(100.0);
        else if(cpuPercent>71)
            ui->curveChartCpu->setMaxValue(80.0);
        else if(cpuPercent>61)
            ui->curveChartCpu->setMaxValue(70.0);
        else if(cpuPercent>51)
            ui->curveChartCpu->setMaxValue(60.0);
        else if(cpuPercent>41)
            ui->curveChartCpu->setMaxValue(50.0);
        else if(cpuPercent>31)
            ui->curveChartCpu->setMaxValue(40.0);
        else if(cpuPercent>21)
            ui->curveChartCpu->setMaxValue(30.0);
        else if(cpuPercent>10)
            ui->curveChartCpu->setMaxValue(20.0);



        if(memoryPercent>81)
            ui->curveChartMem->setMaxValue(100.0);
        else if(memoryPercent>71)
            ui->curveChartMem->setMaxValue(80.0);
        else if(memoryPercent>61)
            ui->curveChartMem->setMaxValue(70.0);
        else if(memoryPercent>51)
            ui->curveChartMem->setMaxValue(60.0);
        else if(memoryPercent>31)
            ui->curveChartMem->setMaxValue(50.0);
        else if(memoryPercent>21)
            ui->curveChartMem->setMaxValue(30.0);
        else if(memoryPercent>10)
            ui->curveChartMem->setMaxValue(20.0);

    });

}

void FrameState::drawCpuAndMem(int cpuPercent, int memoryPercent, int memoryAll, int memoryUse, int memoryFree)
{
    ui->curveChartCpu->addData(cpuPercent);
    ui->curveChartMem->addData(memoryPercent);
    ui->progressBarRoundCpu->setValue(cpuPercent);
    ui->progressBarRoundMem->setValue(memoryPercent);
}
