﻿#ifndef FRMMAP_H
#define FRMMAP_H

#include <QWidget>
#include <QKeyEvent>
#include <QLabel>
#include<Kernel/DataForwarding.h>
#include"FrameMain.h"

class QPushButton;

namespace Ui {
class FrameVideoPage;
}

class FrameVideoPage : public QWidget
{
    Q_OBJECT

public:
    explicit FrameVideoPage(QWidget *parent = 0);
    ~FrameVideoPage();
    void setDataForwardingCore(DataForwarding* dataForwardingCore);
    void setFather(FrameMain* father);
    QTimer *timer;
private:
    FrameMain* father;
    Ui::FrameVideoPage *ui;
    void keyPressEvent(QKeyEvent *event);
    DataForwarding* dataForwardingCore;
     const QRectF & geometry = QRectF();

private slots:
    void initForm();
    void on_switchButton_checkedChanged(bool checked);
    void scrnNormal();

};

#endif // FRMMAP_H
