﻿#include "FrameMain.h"
#include "ui_FrameMain.h"
#include "BasicFunction/myhelper.h"
#include "FrameVideoPage.h"
#include "FrameSetting.h"
#include "FrameState.h"
#include "DataShow/tablewidget.h"

FrameMain::FrameMain(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FrameMain)
{

    ui->setupUi(this);


    dataForwardingCore = new DataForwarding;
    //ui->btnExit->setIcon(iconExit3d);


    this->initStyle();
    this->initForm();
    myHelper::formInCenter(this);

    if (App::UseStyle) {
        on_btnMenu_Min_clicked();
    } else {
        this->showMaximized();
    }
}

FrameMain::~FrameMain()
{
    delete ui;
}

bool FrameMain::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == ui->widget_title) {
        if (event->type() == QEvent::MouseButtonDblClick) {
            this->on_btnMenu_Max_clicked();
            return true;
        }
    }

    return QWidget::eventFilter(obj, event);
}

void FrameMain::initStyle()
{
    IconHelper::Instance()->setIconClose(ui->btnMenu_Close);
    IconHelper::Instance()->setIconNormal(ui->btnMenu_Max);
    IconHelper::Instance()->setIconMin(ui->btnMenu_Min);


    IconHelper::Instance()->setIconMain(ui->lab_Ico, 40);
    ui->lab_Title->setFont(QFont(App::FontName, 18));
    //ui->lab_Ico->setFixedWidth(80);
    //ui->labCPUMemory->setFixedWidth(380);
    //ui->lcdNumber->setFixedWidth(380);


    if (App::UseStyle) {
        //设置主页面图标
        ChangeMainIconSlot(App::MainUiIcon);

        this->max = false;
        this->location = this->geometry();
        this->setProperty("Form", true);
        this->setProperty("CanMove", true);
        this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);
        connect(ui->btnMenu_Close, SIGNAL(clicked()), this, SLOT(close()));
    } else {
        this->setWindowFlags(Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint);
        this->resize(size().width(), size().height() - ui->widget_title->size().height());
    }

    ui->widget_title->installEventFilter(this);
    //ui->labCPUMemory->setVisible(false);
    //ui->lcdNumber->setVisible(false);
    //ui->widget_menu->setVisible(false);

    ui->lab_Title->setText(App::Title);
    this->setWindowTitle(ui->lab_Title->text());
}

void FrameMain::initForm()
{

    FrameVideoPage* fvp = new FrameVideoPage;
    fvp->setDataForwardingCore(this->dataForwardingCore);
    fvp->setFather(this);

    ui->stackedWidget->addWidget(fvp);

    FrameSetting* fs = new FrameSetting;
    ui->stackedWidget->addWidget(fs);

    ui->stackedWidget->addWidget(new FrameState );
    ui->stackedWidget->addWidget(new TableWidget);
    //切换主界面图标信号处理连接
    connect(fs,SIGNAL(MainIconStyle(int)),this,SLOT(ChangeMainIconSlot(int)));


    int width = 20;
    int interval = 1000;


    if (App::UseStyle) {
        QStringList qss;
        qss.append(QString("QComboBox::drop-down,QDateEdit::drop-down,QTimeEdit::drop-down,QDateTimeEdit::drop-down{width:%1px;}").arg(width));
        qss.append("QListWidget::item,QTreeView::item{padding:3px;margin:0px;}");
        this->setStyleSheet(qss.join(""));
    }

    QString qss = QString("QLabel,QLCDNumber{background-color:rgb(0,0,0);color:rgb(%1);}").arg("255,107,107");


    ui->widget_bottom->setTitle(App::Title);
    ui->widget_bottom->setLineFixedWidth(App::UseStyle);
    ui->widget_bottom->start();

    QList<QToolButton *> btns = ui->widget_top->findChildren<QToolButton *>();


    QSize icoSize(50, 50);
    int icoWidth = 85;


    foreach (QToolButton *btn, btns) {
        btn->setCheckable(true);
        //btn->setIconSize(icoSize);
        btn->setMinimumWidth(icoWidth);
        connect(btn, SIGNAL(clicked()), this, SLOT(buttonClick()));
    }

    ui->btnMain->click();
}

void FrameMain::buttonClick()
{
    QToolButton *btn = (QToolButton *)sender();
    QString name = btn->text();
    buttonCheck(btn);

    if (name == "监控") {
        ui->stackedWidget->setCurrentIndex(0);
    } else if (name == "配置") {
        ui->stackedWidget->setCurrentIndex(1);
    } else if (name == "状态") {
        ui->stackedWidget->setCurrentIndex(2);
    } else if (name == "数据") {
        ui->stackedWidget->setCurrentIndex(3);
    } else if (name == "退出") {
        close();
    }
}

void FrameMain::buttonCheck(QToolButton *btn)
{
    QList<QToolButton *> btns = ui->widget_top->findChildren<QToolButton *>();

    foreach (QToolButton *b, btns) {
        if (b == btn) {
            b->setChecked(true);
        } else {
            b->setChecked(false);
        }
    }
}

void FrameMain::MinScreen()
{
    //this->setGeometry();
    this->setGeometry(location);
    myHelper::formInCenter(this);
}

void FrameMain::on_btnMenu_Min_clicked()
{
    if (App::UseTray) {
        QString msg = "程序已经最小化到后台运行!";
        trayIcon->showMessage(App::Title, msg, QSystemTrayIcon::Information, 1);
        this->hide();
    } else {
        this->showMinimized();
    }
}

void FrameMain::on_btnMenu_Max_clicked()
{
    if (max) {

        this->setGeometry(location);
        myHelper::formInCenter(this);

        IconHelper::Instance()->setIconNormal(ui->btnMenu_Max);
        ui->btnMenu_Max->setToolTip("最大化");
        this->setProperty("CanMove", true);
    } else {
        //location = this->geometry();

        this->setGeometry(qApp->desktop()->availableGeometry());
        IconHelper::Instance()->setIconMax(ui->btnMenu_Max);
        ui->btnMenu_Max->setToolTip("还原");
        this->setProperty("CanMove", false);
    }

    max = !max;
}

void FrameMain::iconIsActived(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick: {
        showNormal();
        break;
    }

    default:
        break;
    }
}

//切换界面图标风格
void FrameMain::ChangeMainIconSlot(int which)
{
    if(which == 2)
    {
        QIcon iconData2d(QString(":/banner/pie_chart_128px_1132347_easyicon.net.png"));
        QIcon iconMain2d(QString(":/banner/eye_128px_1132293_easyicon.net.png"));
        QIcon iconState2d(QString(":/banner/barchart_128px_1132241_easyicon.net.png"));
        QIcon iconExit2d(QString(":/banner/stop_128px_1132391_easyicon.net.png"));
        QIcon iconHelp2d(QString(":/banner/star_128px_1132389_easyicon.net.png"));
        QIcon iconConfig2d(QString(":/banner/gear_128px_1132307_easyicon.net.png"));
        ui->btnData->setIcon(iconData2d);
        ui->btnExit->setIcon(iconExit2d);
        ui->btnMain->setIcon(iconMain2d);
        ui->btnConfig->setIcon(iconConfig2d);
        ui->btnHelp->setIcon(iconHelp2d);
        ui->btnState->setIcon(iconState2d);

    }
    else if(which == 1)
    {
        QIcon iconData3d(QString(":/image/analytics_256px_1159836_easyicon.net.png"));
        QIcon iconMain3d(QString(":/image/Video_512px_1139972_easyicon.net.png"));
        QIcon iconState3d(QString(":/image/TV_512px_1139971_easyicon.net.png"));
        QIcon iconExit3d(QString(":/image/close.png"));
        QIcon iconHelp3d(QString(":/image/Notebook_512px_1139965_easyicon.net.png"));
        QIcon iconConfig3d(QString(":/image/Seting_512px_1139969_easyicon.net.png"));
        ui->btnData->setIcon(iconData3d);
        ui->btnExit->setIcon(iconExit3d);
        ui->btnMain->setIcon(iconMain3d);
        ui->btnConfig->setIcon(iconConfig3d);
        ui->btnHelp->setIcon(iconHelp3d);
        ui->btnState->setIcon(iconState3d);
    }
}
