﻿#ifndef FRMMAIN_H
#define FRMMAIN_H

#include <QDialog>
#include <QSystemTrayIcon>
#include<Kernel/DataForwarding.h>

class QToolButton;

namespace Ui {
class FrameMain;
}

class FrameMain : public QDialog
{
    Q_OBJECT

public:
    explicit FrameMain(QWidget *parent = 0);
    ~FrameMain();

protected:
    bool eventFilter(QObject *obj, QEvent *event);

private:
    Ui::FrameMain *ui;
    bool max;
    QRect location;
    QSystemTrayIcon *trayIcon;

    //网络通信、数据转发等功能
    DataForwarding* dataForwardingCore;


private slots:
    void initStyle();
    void initForm();
    void buttonClick();
    void buttonCheck(QToolButton *btn);

public slots:
    void MinScreen();

private slots:
    void on_btnMenu_Min_clicked();
    void on_btnMenu_Max_clicked();
    void iconIsActived(QSystemTrayIcon::ActivationReason reason);
    void ChangeMainIconSlot(int which);
};

#endif // FRMMAIN_H
