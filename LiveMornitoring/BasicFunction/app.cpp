﻿#include "app.h"
#include "myhelper.h"

int App::IconMain = 0xf01d;
int App::IconMenu = 0xf0d7;
int App::IconMin = 0xf068;
int App::IconMax = 0xf066;
int App::IconNormal = 0xf096;
int App::IconClose = 0xf00d;

bool App::UseTray = false;
bool App::SaveLog = false;
bool App::UseStyle = true;
QString App::StyleName = ":/qss/gray.css";
int App::FontSize = 9;
QString App::FontName = "Microsoft YaHei";
QString App::Title = "直播监控";


int App::MainUiIcon = 1;              //默认采用1号样式 3D化
bool App::hasFontSizeLocked = true;
bool App::hasFontTypeLocked = true;
bool App::hasCustomStyleLocked = false;
QString App::imgSavePath = QString("%1/SaveImages").arg(AppPath);
bool App::autoBoot = false;
bool App::BootMaxWindow = false;
bool App::BootfullScreen = false;
bool App::spaceAlert = false;
bool App::abnormalAlert = false;
bool App::signalAlert = false;
bool App::playerAlert = false;
bool App::showCurrentTime = false;
bool App::showCurrentUser = true;
bool App::showSystemRunningTime = true;
bool App::showTitleinLeftBottom = true;

int App::ScrnState = 0;//0表示最下窗口  1表示全屏  2表示单个视频全屏
int App::intervalFrames = 25;
Qt::TransformationMode App::HQVideo = Qt::FastTransformation;

//Tensorflow所需参数
double App::Recall = 0.1;

//非永久存储全局变量
int  App::ClientNum = 0;
QString  App::MSG_TYPE_FEATURE = "1";
QString  App::MSG_TYPE_UPDATE_LIST = "2";


void App::ReadConfig()
{
	if (!CheckConfig()) {
		return;
	}

	QString fileName = QString("%1/%2_Config.ini").arg(AppPath).arg(AppName);
	QSettings set(fileName, QSettings::IniFormat);


	set.beginGroup("AppConfig");
        App::UseTray = set.value("UseTray").toBool();
        App::SaveLog = set.value("SaveLog").toBool();
        App::UseStyle = set.value("UseStyle").toBool();
        App::StyleName = set.value("StyleName").toString();
        App::FontSize = set.value("FontSize").toInt();
        App::FontName = set.value("FontName").toString();

        set.beginGroup("UiStyleConfig");
            App::MainUiIcon = set.value("MainUiIcon").toInt();
            App::hasCustomStyleLocked = set.value("hasCustomStyleLocked").toBool();
            App::hasFontSizeLocked = set.value("hasFontSizeLocked").toBool();
            App::hasFontTypeLocked = set.value("hasFontTypeLocked").toBool();
            App::hasCustomStyleLocked = set.value("hasCustomStyleLocked").toBool();
        set.endGroup();

        set.beginGroup("FunctionConfig");
            App::imgSavePath = set.value("imgSavePath").toString();
            App::autoBoot = set.value("autoBoot").toBool();
            App::BootMaxWindow = set.value("BootMaxWindow").toBool();
            App::BootfullScreen = set.value("BootfullScreen").toBool();
            App::spaceAlert = set.value("spaceAlert").toBool();
            App::abnormalAlert = set.value("abnormalAlert").toBool();
            App::signalAlert = set.value("signalAlert").toBool();
            App::playerAlert = set.value("playerAlert").toBool();
            App::showCurrentTime = set.value("showCurrentTime").toBool();
            App::showCurrentUser = set.value("showCurrentUser").toBool();
            App::showSystemRunningTime = set.value("showSystemRunningTime").toBool();
            App::showTitleinLeftBottom = set.value("showTitleinLeftBottom").toBool();
        set.endGroup();

	set.endGroup();

	set.beginGroup("MainConfig");
	App::Title = set.value("Title").toString();
	set.endGroup();
}

void App::WriteConfig()
{
	QString fileName = QString("%1/%2_Config.ini").arg(AppPath).arg(AppName);
	QSettings set(fileName, QSettings::IniFormat);

	set.beginGroup("AppConfig");
        set.setValue("UseTray", App::UseTray);
        set.setValue("SaveLog", App::SaveLog);
        set.setValue("UseStyle", App::UseStyle);
        set.setValue("StyleName", App::StyleName);
        set.setValue("FontSize", App::FontSize);
        set.setValue("FontName", App::FontName);

        set.beginGroup("UiStyleConfig");
              set.setValue("MainUiIcon",App::MainUiIcon);
              set.setValue("hasCustomStyleLocked",App::hasCustomStyleLocked);
              set.setValue("hasFontSizeLocked",App::hasFontSizeLocked);
              set.setValue("hasFontTypeLocked",App::hasFontTypeLocked);
              set.setValue("hasCustomStyleLocked",App::hasCustomStyleLocked);
        set.endGroup();

        set.beginGroup("FunctionConfig");
             set.setValue("imgSavePath",App::imgSavePath);
             set.setValue("autoBoot",App::autoBoot);
             set.setValue("BootMaxWindow",App::BootMaxWindow);
             set.setValue("BootfullScreen",App::BootfullScreen);
             set.setValue("spaceAlert",App::spaceAlert);
             set.setValue("abnormalAlert",App::abnormalAlert);
             set.setValue("signalAlert",App::signalAlert);
             set.setValue("playerAlert",App::playerAlert);
             set.setValue("showCurrentTime",App::showCurrentTime);
             set.setValue("showCurrentUser",App::showCurrentUser);
             set.setValue("showSystemRunningTime",App::showSystemRunningTime);
             set.setValue("showTitleinLeftBottom",App::showTitleinLeftBottom);
        set.endGroup();

	set.endGroup();




	set.beginGroup("MainConfig");
	set.setValue("Title", App::Title);
	set.endGroup();
}

void App::NewConfig()
{
#if (QT_VERSION <= QT_VERSION_CHECK(5,0,0))
	App::Title = App::Title.toLatin1();
#endif
	WriteConfig();
}

bool App::CheckConfig()
{
	QString fileName = QString("%1/%2_Config.ini").arg(AppPath).arg(AppName);

	//如果配置文件大小为0,则以初始值继续运行,并生成配置文件
	QFile file(fileName);

	if (file.size() == 0) {
		NewConfig();
		return false;
	}

	//如果配置文件不完整,则以初始值继续运行,并生成配置文件
	if (file.open(QFile::ReadOnly)) {
		bool ok = true;

		while (!file.atEnd()) {
			QString line = file.readLine();
			line = line.replace("\r", "");
			line = line.replace("\n", "");
			QStringList list = line.split("=");

			if (list.count() == 2) {
				if (list.at(1) == "") {
					ok = false;
					break;
				}
			}
		}

		if (!ok) {
			NewConfig();
			return false;
		}
	} else {
		NewConfig();
		return false;
	}

	return true;
}

void App::WriteError(QString str)
{
	QString fileName = QString("%1/log/%2_Error_%3.txt").arg(AppPath).arg(AppName).arg(QDATE);
	QFile file(fileName);
	file.open(QIODevice::WriteOnly | QIODevice::Append | QFile::Text);
	QTextStream stream(&file);
	stream << DATETIME << "  " << str << "\n";
}

void App::WriteStartTime()
{
	QString fileName = QString("%1/log/%2_Start_%3.txt").arg(AppPath).arg(AppName).arg(QDATE);
	QFile file(fileName);
	QString str = QString("start time : %1").arg(DATETIME);
	file.open(QFile::WriteOnly | QIODevice::Append | QFile::Text);
	QTextStream stream(&file);
	stream << str << "\n";
}

void App::NewDir(QString dirName)
{
	//如果路径中包含斜杠字符则说明是绝对路径
	//windows系统 路径字符带有 :/  linux系统路径字符带有 /
	if (!dirName.contains(":/") && !dirName.startsWith("/")) {
		dirName = QString("%1/%2").arg(AppPath).arg(dirName);
	}

	QDir dir(dirName);

	if (!dir.exists()) {
		dir.mkpath(dirName);
	}
}
