﻿#ifndef APP_H
#define APP_H

class QString;

class App
{
public:
    static int IconMain;                //左上角图标
    static int IconMenu;                //下来菜单图标
    static int IconMin;                 //最小化图标
    static int IconMax;                 //最大化图标
    static int IconNormal;              //正常图标
    static int IconClose;               //关闭图标

    static bool UseTray;                //启用托盘
    static bool SaveLog;                //输出日志文件
    static bool UseStyle;               //启用样式
    static QString StyleName;           //应用程序样式
    static int FontSize;                //应用程序字体大小
    static QString FontName;            //应用程序字体名称

    static QString Title;               //标题

    static int MainUiIcon;              //主界面图标样式
    static bool hasFontSizeLocked;
    static bool hasFontTypeLocked;
    static bool hasCustomStyleLocked;
    static QString imgSavePath;
    static bool autoBoot;
    static bool BootMaxWindow;
    static bool BootfullScreen;
    static bool spaceAlert;
    static bool abnormalAlert;
    static bool signalAlert;
    static bool playerAlert;
    static bool showCurrentTime;
    static bool showCurrentUser;
    static bool showSystemRunningTime;
    static bool showTitleinLeftBottom;

    static int ScrnState;
    static int intervalFrames;//播放器播放视频时 每播一帧所跳过的帧
    static Qt::TransformationMode HQVideo;

    static void ReadConfig();           //读取配置文件,在main函数最开始加载程序载入
    static void WriteConfig();          //写入配置文件,在更改配置文件程序关闭时调用
    static void NewConfig();            //以初始值新建配置文件
    static bool CheckConfig();          //校验配置文件

    static void WriteError(QString str);//写入错误信息
    static void WriteStartTime();       //写入启动时间
    static void NewDir(QString dirName);//新建目录



    static int ClientNum;
    static QString MSG_TYPE_UPDATE_LIST;
    static QString MSG_TYPE_FEATURE;
    static double Recall;
};

#endif // APP_H
