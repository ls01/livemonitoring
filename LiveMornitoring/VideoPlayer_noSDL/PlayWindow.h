#ifndef PLAYWINDOW_H
#define PLAYWINDOW_H

#include <QWidget>
#include <QFileDialog>
#include <QSlider>
#include <QHBoxLayout>
#include <QPushButton>
#include <QDebug>
#include <QTcpSocket>

#include "videoplayer.h"
class PlayWindow : public QWidget
{
    Q_OBJECT
public:
    explicit PlayWindow(QWidget *parent = 0);
    ~PlayWindow();

signals:

private slots:
    void slotgetFrame(QImage);
    void slotValueChanged(int);
    void slotUpdateTime(long time);
    void slotOpenBtnClicked();
    void slotPlayBtnClicked();
    void slotStateChanged(VideoPlayer::State);

    void readMesg();


private:
    VideoPlayer *player;
    QLabel *m_lab_show;
    QPushButton *m_btn_play;
    QPushButton *m_btn_open;
    QSlider *m_horizontalSlider;

    QTcpSocket *m_tcpSocket;
    QByteArray firstByteArray;
    QByteArray okByteArray;
    QByteArray notOkByteArray;
    int flag;
    bool b_openStart;
    int count;

    int num;


};

#endif // PLAYWINDOW_H
