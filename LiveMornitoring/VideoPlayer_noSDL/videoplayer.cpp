#include "videoplayer.h"


/***
 ***DecodeVideo类的成员
 ***/
static QMutex decodeVideoMutex;
#if 0
DecodeVideo::DecodeVideo()
{
    bufferRGB = NULL;
}
DecodeVideo::~DecodeVideo()
{

}
void DecodeVideo::setAVCodecContext(AVCodecContext*ctx)
{
    pCodecCtx = ctx;
    width = pCodecCtx->width;
    height= pCodecCtx->height;
    pix_fmt = pCodecCtx->pix_fmt;
    if (bufferRGB != NULL)
    {
        av_free(bufferRGB);
        bufferRGB = NULL;
    }
    int numBytes = avpicture_get_size(PIX_FMT_RGB24, pCodecCtx->width,pCodecCtx->height);
    bufferRGB = (uint8_t *)av_malloc(numBytes*sizeof(uint8_t));


}

void DecodeVideo::run()
{
    int frameFinished = 0;
    AVFrame *pFrame = avcodec_alloc_frame();
    decodeVideoMutex.lock();
    avcodec_decode_video(pCodecCtx, pFrame, &frameFinished,packet.data,packet.size);
    decodeVideoMutex.unlock();

    AVFrame *pFrameRGB;
    pFrameRGB = avcodec_alloc_frame();
    avpicture_fill((AVPicture *)pFrameRGB, bufferRGB, PIX_FMT_RGB24,pCodecCtx->width, pCodecCtx->height);
    SwsContext *convert_ctx = sws_getContext(width,height,pix_fmt,width,height,PIX_FMT_RGB24,SWS_BICUBIC, NULL,NULL,NULL);
    sws_scale(convert_ctx,(const uint8_t*  const*)pFrame->data,pFrame->linesize,0,height,pFrameRGB->data,pFrameRGB->linesize);
    QImage tmpImage((uchar *)bufferRGB,width,height,QImage::Format_RGB888);
    QImage image  = tmpImage.copy();
    av_free(pFrameRGB);
    sws_freeContext(convert_ctx);
    emit readOneFrame(image);

    av_free_packet(&packet);
}
#endif

/***
 ***VideoPlayer类的成员
 ***/
VideoPlayer::VideoPlayer()
{
    initAvcodec();

    aCodecCtx  = NULL;
    pFormatCtx = NULL;
    eventloop  = NULL;
    curState = StoppedState;
    curType = NoneType;

//    decodeVideoThread = new DecodeVideo;
//    connect(decodeVideoThread,SIGNAL(readOneFrame(QImage)),this,SIGNAL(readOneFrame(QImage)));

}

VideoPlayer::~VideoPlayer()
{

}

void VideoPlayer::run()
{
    eventloop = new QEventLoop;
    QTimer playtimer; //控制播放的定时器
    connect(&playtimer,SIGNAL(timeout()),this,SLOT(readPacket()),Qt::DirectConnection);
    playtimer.start(50);
    eventloop->exec();
    delete eventloop;
    eventloop = NULL;
}

void VideoPlayer::initAvcodec()
{
    av_register_all();
    avcodec_init();
    avcodec_register_all();
    avdevice_register_all();
}

bool VideoPlayer::openVideo(char *filename)
{
    videoStream = -1;
    audioStream = -1;
    if(av_open_input_file(&pFormatCtx, filename, NULL, 0, NULL)!=0)
    {
        fprintf(stderr, "Couldn't open file\n");
        return false;  //Couldn't open file
    }
    if(av_find_stream_info(pFormatCtx)<0)
    {
        fprintf(stderr, "Couldn't find stream information\n");
        return false ; // Couldn't find stream information
    }
    //dump_format(pFormatCtx, 0, filename, 0);  //输出视频信息到终端
    int i;
    for(i=0; i<pFormatCtx->nb_streams; i++)
    {
        if(pFormatCtx->streams[i]->codec->codec_type==CODEC_TYPE_VIDEO && videoStream < 0)
        {
            videoStream=i;
        }
        if(pFormatCtx->streams[i]->codec->codec_type==CODEC_TYPE_AUDIO && audioStream < 0)
        {
            audioStream=i;
        }
    }

    if(audioStream==-1 && videoStream==-1)
    {
        closeVideo();
        fprintf(stderr, "Didn't find a audio stream\n");
        return false; // Didn't find a audio stream
    }

    if (videoStream != -1)
    {
        // Get a pointer to the codec context for the video stream
        pCodecCtx=pFormatCtx->streams[videoStream]->codec;
        // Find the decoder for the video stream
        AVCodec *pCodec=avcodec_find_decoder(pCodecCtx->codec_id);
        if(pCodec==NULL)
        {
            fprintf(stderr, "Unsupported codec!\n");
            return false; // Codec not found
        }
        // Open codec
        if(avcodec_open(pCodecCtx, pCodec)<0)
        {
            fprintf(stderr, "Could not open audio codec!\n");
            return false; // Could not open audio codec
        }
        curType = VideoType;
    }
    else
    {
        curType = AudioType;
    }

    if (audioStream != -1)
    {
        aCodecCtx = pFormatCtx->streams[audioStream]->codec;
        AVCodec *aCodec = avcodec_find_decoder(aCodecCtx->codec_id);
        if(!aCodec)
        {
            fprintf(stderr, "Unsupported codec!\n");
            return false;
        }
        if(avcodec_open(aCodecCtx, aCodec)<0)
        {
            fprintf(stderr, "Could not open video codec!\n");
            return false; // Could not open video codec
        }
    }

    totaltime = pFormatCtx->duration;
    return true;
}

void VideoPlayer::closeVideo()
{
    if (aCodecCtx != NULL)
    {
        avcodec_close(aCodecCtx);
    }
    if (pFormatCtx != NULL)
    {
        av_close_input_file(pFormatCtx);
    }
    aCodecCtx  = NULL;
    pFormatCtx = NULL;
    curType = NoneType;
}

//extern QQueue<QByteArray> queueFrame;
void VideoPlayer::readPacket()
{
    if (pFormatCtx == NULL) return;
    mutex.lock();
    currenttime+=10;
    if (currenttime >= nextPacket.dts)
    {
//        if(nextPacket.stream_index == videoStream)
//        if(!queueFrame.isEmpty())
        {
//            QByteArray getFram = queueFrame.dequeue();
            packet = nextPacket;
//            qDebug()<<packet.size<<packet.date[0]<<packet.date[1]<<packet.date[2]<<packet.date[3]<<packet.date[4]<<packet.date[5];

            qDebug("%d:[%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x]",
                   packet.size,
                   packet.data[0],
                    packet.data[1],
                    packet.data[2],
                    packet.data[3],
                    packet.data[4],
                    packet.data[5],
                    packet.data[6],
                    packet.data[7],
                    packet.data[8],
                    packet.data[9],
                    packet.data[10],
                    packet.data[11],
                    packet.data[12],
                     packet.data[13],
                     packet.data[14],
                     packet.data[15],
                     packet.data[16],
                     packet.data[17],
                     packet.data[18],
                     packet.data[19],
                     packet.data[20],
                     packet.data[21],
                     packet.data[22],
                     packet.data[23]
                    );

            int frameFinished = 0;
            AVFrame *pFrame = avcodec_alloc_frame();
            decodeVideoMutex.lock();
            avcodec_decode_video(pCodecCtx, pFrame, &frameFinished,packet.data,packet.size);
//            avcodec_decode_video(pCodecCtx, pFrame, &frameFinished,(uint8_t *)getFram.data(),getFram.size());
            decodeVideoMutex.unlock();

            AVFrame *pFrameRGB;
            pFrameRGB = avcodec_alloc_frame();
            avpicture_fill((AVPicture *)pFrameRGB, bufferRGB, PIX_FMT_RGB24,pCodecCtx->width, pCodecCtx->height);
            SwsContext *convert_ctx = sws_getContext(width,height,pix_fmt,width,height,PIX_FMT_RGB24,SWS_BICUBIC, NULL,NULL,NULL);
            sws_scale(convert_ctx,(const uint8_t*  const*)pFrame->data,pFrame->linesize,0,height,pFrameRGB->data,pFrameRGB->linesize);
            QImage tmpImage((uchar *)bufferRGB,width,height,QImage::Format_RGB888);
            QImage image  = tmpImage.copy();
            av_free(pFrameRGB);
            sws_freeContext(convert_ctx);
            emit readOneFrame(image);

//            av_free_packet(&packet);

        }
        if(av_read_frame(pFormatCtx, &nextPacket) < 0)
        {//整个视频文件读取完毕
            stop();
            currenttime = totaltime;
            emit updateTime(currenttime);
            emit finished();
        }
    }
    mutex.unlock();
}

void VideoPlayer::setSource(QString str)
{
    stop();
    char ch[1024];
    strcpy(ch,(const char*)str.toLocal8Bit());
    if (openVideo(ch))
    {
        currenttime = 0;
        av_read_frame(pFormatCtx, &nextPacket);
        if (curType == VideoType)
        {
//            decodeVideoThread->setAVCodecContext(pCodecCtx);

//            pCodecCtx = pCodecCtx;
            width = pCodecCtx->width;
            height= pCodecCtx->height;
            pix_fmt = pCodecCtx->pix_fmt;
            if (bufferRGB != NULL)
            {
                av_free(bufferRGB);
                bufferRGB = NULL;
            }
            int numBytes = avpicture_get_size(PIX_FMT_RGB24, pCodecCtx->width,pCodecCtx->height);
            bufferRGB = (uint8_t *)av_malloc(numBytes*sizeof(uint8_t));

        }
    }
    else
    {
        fprintf(stderr,"open %s erro!\n",ch);
    }
}

void VideoPlayer::play()
{
    if (isRunning()) return;
    if (pFormatCtx != NULL)
    {
//        openSDL();
        start();
        curState = PlayingState;
        emit stateChanged(curState);
    }
}

void VideoPlayer::pause()
{
    if (eventloop == NULL) return;
    eventloop->exit();
//    closeSDL();
    curState = PausedState;
    emit stateChanged(curState);
}

void VideoPlayer::stop()
{
    if (eventloop == NULL) return;
    eventloop->exit();
    closeVideo();
//    closeSDL();

    curState = StoppedState;
    emit stateChanged(curState);
}

void VideoPlayer::seek(qint64 time)
{

}


