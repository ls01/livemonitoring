#include "PlayWindow.h"

QQueue<QByteArray> queueFrame;
PlayWindow::PlayWindow(QWidget *parent) : QWidget(parent)
{
    this->setFixedSize(630,400);
    m_lab_show = new QLabel;
    m_btn_open = new QPushButton;
    m_btn_play = new QPushButton;
    m_horizontalSlider = new QSlider(Qt::Horizontal);

    QHBoxLayout *mLayout = new QHBoxLayout;
    mLayout->addWidget(m_horizontalSlider,80,Qt::AlignCenter);
    mLayout->addWidget(m_btn_play,10,Qt::AlignCenter);
    mLayout->addWidget(m_btn_open,10,Qt::AlignCenter);

    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(m_lab_show,90,Qt::AlignCenter);
    mainLayout->addLayout(mLayout,10);
    mainLayout->setMargin(0);
    m_lab_show->setFixedSize(640,360);
    m_horizontalSlider->setFixedSize(500,20);
    m_btn_play->setFixedSize(60,30);
    m_btn_open->setFixedSize(60,30);
    m_btn_play->setText("暂停");
    m_btn_open->setText("打开");



    player = new VideoPlayer;

    connect(player,SIGNAL(readOneFrame(QImage)),this,SLOT(slotgetFrame(QImage)));
    connect(player,SIGNAL(updateTime(long)),this,SLOT(slotUpdateTime(long)));
    connect(player,SIGNAL(stateChanged(VideoPlayer::State)),this,SLOT(slotStateChanged(VideoPlayer::State)));


    m_horizontalSlider->setRange(0,100);

    connect(m_horizontalSlider,SIGNAL(sliderMoved(int)),this,SLOT(slotValueChanged(int)));
    connect(m_btn_open,SIGNAL(clicked()),this,SLOT(slotOpenBtnClicked()));
    connect(m_btn_play,SIGNAL(clicked()),this,SLOT(slotPlayBtnClicked()));



    m_tcpSocket = new QTcpSocket(this);
    connect(m_tcpSocket,SIGNAL(readyRead()),this,SLOT(readMesg()));


    flag = 0;
    b_openStart = false;

    m_tcpSocket->connectToHost("127.0.0.1",5050);

    count = 0;

}

PlayWindow::~PlayWindow()
{

}

void PlayWindow::slotgetFrame(QImage image)
{
    QPixmap pixmap = QPixmap::fromImage(image.scaled(m_lab_show->size(), Qt::KeepAspectRatio) );
    m_lab_show->setPixmap(pixmap);
}

void PlayWindow::slotValueChanged(int value)
{
    qint64 v = (value/100.0)*player->totalTime();
    player->seek(v);
}

void PlayWindow::slotUpdateTime(long time)
{
    double v = time*1000.0/player->totalTime()*100.0;
    m_horizontalSlider->setValue(v);
}

void PlayWindow::slotOpenBtnClicked()
{
    QString s = QFileDialog::getOpenFileName(
        this, "open file dialog",
            "/",//初始目录
         "All files (*);;Movie files (*.rmvb *.flv *.mp3 *.wmv *.wma)");
       if (!s.isEmpty())
    {
           m_lab_show->clear();




           player->setSource(s);
           player->play();
    }
//       player->setSource("E:\WorkSpace\CSDN_Git\QT_ffmpeg\build-RTSPTool-Desktop_Qt_5_4_1_MinGW_32bit-Release\bin\videoFile");
//       player->play();
}

void PlayWindow::slotPlayBtnClicked()
{
    if (player->state() == VideoPlayer::PlayingState)
    {
        player->pause();
    }
    else if (player->state() == VideoPlayer::PausedState)
    {
        player->play();
    }
}

void PlayWindow::slotStateChanged(VideoPlayer::State state)
{
    if (state == VideoPlayer::PlayingState)
    {
        m_btn_play->setEnabled(true);
        m_btn_play->setText("暂停");
    }
    if (state == VideoPlayer::PausedState)
    {
        m_btn_play->setText("播放");
    }
    if (state == VideoPlayer::StoppedState)
    {
        m_btn_play->setEnabled(false);
        m_btn_play->setText("播放");
    }
}

void PlayWindow::readMesg() //读取信息
{
    QByteArray msg = m_tcpSocket->readAll();

#if 0
    FILE *fp = fopen("./videoFile","ab+");
    fwrite((unsigned char *)msg.data(),msg.size(),1,fp);
    fclose(fp);


    count++;

    qDebug()<<"~~~~~~~~"<<count;
    if(b_getFirst && count == 10)
    {
        b_getFirst = false;
        player->setSource("./videoFile");
        player->play();
        qDebug()<<"~~~~open~~~";
        count = 0;
    }
    else
    {
    }

#endif

    QByteArray tmpMsg;
    tmpMsg.clear();

    if(num < 20)
    {
        num++;

        FILE *fp = fopen("./videoFile","ab+");
        fwrite((unsigned char *)msg.data(),msg.size(),1,fp);
        fclose(fp);

    }
    else if(!b_openStart)
    {
        b_openStart = true;
        for(int i = 0; i < msg.length(); i++)
        {
    //        qDebug("[%02x]-----------:%d\n", (unsigned char )msg.data()[i], i);
            if(msg.data()[i] == 0x00
                && msg.data()[i+1] == 0x00
                && msg.data()[i+2] == 0x00
                && msg.data()[i+3] == 0x01)
            {
                flag = i;
                break;
            }
            else
            {
                tmpMsg.append(msg.data()[i]);
            }
        }
        FILE *fp = fopen("./videoFile","ab+");
        fwrite((unsigned char *)tmpMsg.data(),tmpMsg.size(),1,fp);
        fclose(fp);

        player->setSource("./videoFile");
        player->play();
        qDebug()<<"~~~~open~~~";



    }
    else if(b_openStart)
    {
    }



    for(int i = 0; i < msg.length(); i++)
    {
//        qDebug("[%02x]-----------:%d\n", (unsigned char )msg.data()[i], i);
        if(msg.data()[i] == 0x00
            && msg.data()[i+1] == 0x00
            && msg.data()[i+2] == 0x00
            && msg.data()[i+3] == 0x01)
        {
            if(0 == flag)
            {
                flag = 1;
                qDebug()<<"===========0";
                notOkByteArray.append(msg.data()[i]);
                count++;
            }
            else
            {
                if(num < 10)
                {
                    num++;
//                    QFile file("./videoFile");
//                    if (file.open(QIODevice::WriteOnly))
//                    {
//                        QTextStream in(&file);
//                        in<<notOkByteArray;
//                    }
//                    file.close();

                    FILE *fp = fopen("./videoFile","ab+");
                    fwrite((unsigned char *)notOkByteArray.data(),msg.size(),1,fp);
                    fclose(fp);

                    player->setSource("./videoFile");
                    player->play();
                    qDebug()<<"~~~~open~~~"<<notOkByteArray.length();

                }
                else
                {
                    okByteArray.append(notOkByteArray);
                    queueFrame.enqueue(okByteArray);
                    qDebug()<<"~~~ok~~~"<<okByteArray.length();

                }

                okByteArray.clear();
                notOkByteArray.clear();
                notOkByteArray.append(msg.data()[i]);
            }
        }
        else
        {
            if(1 == flag)
            {
//                qDebug()<<"===========2";
                notOkByteArray.append(msg.data()[i]);
                count++;
            }
        }
    }

//    QString ss=QVariant(qba).toString();
}
