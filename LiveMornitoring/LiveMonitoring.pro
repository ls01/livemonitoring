#-------------------------------------------------
#
# Project created by QtCreator 2016-09-29T15:15:28
#
#-------------------------------------------------

QT       += core gui network sql xml charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET              = LiveMonitoring
TEMPLATE            = app

win32:RC_FILE       = other/main.rc
PRECOMPILED_HEADER  = BasicFunction/myhelper.h

include($$PWD/BasicFunction/BasicFunction.pri)
include($$PWD/FrameUI/FrameUI.pri)
include($$PWD/usercontrol/usercontrol.pri)
include($$PWD/Kernel/Kernel.pri)
include($$PWD/VideoPlayerCore/VideoPlayerCore.pri)
include($$PWD/DataShow/DataShow.pri)
win32:CONFIG(release, debug|release): LIBS += -L'C:/Program Files (x86)/OpenCV/opencv/build/x64/vc14/lib/' -lopencv_world320
else:win32:CONFIG(debug, debug|release): LIBS += -L'C:/Program Files (x86)/OpenCV/opencv/build/x64/vc14/lib/' -lopencv_world320d

INCLUDEPATH += 'C:/Program Files (x86)/OpenCV/opencv/build/include/opencv2'
DEPENDPATH += 'C:/Program Files (x86)/OpenCV/opencv/build/include/opencv2'


SOURCES     += main.cpp
RESOURCES   += other/main.qrc
INCLUDEPATH += $$PWD
INCLUDEPATH += $$PWD/usercontrol
CONFIG      += qt ##warn_off




