#ifndef DATAFORWARDING_H
#define DATAFORWARDING_H

#include <QObject>
#include <QWidget>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QMutex>
#include <Kernel/qblockpriorityqueue.h>
#include <Kernel/AnomalousEvent.h>
#include <QTime>

//通信报文
struct GeneralMessage{

    int len;
    QString MESG_TYPE;
    int VideoID;
    QTime SendTime;
    double result;
    QVector<double> feature;
    QMap<int,QString> VideoList;
};




class TensorFlow{
public:
    QVector<double> calc(double num){
        QVector<double> res;
        for(int i = 0;i<100;i++){
            res.push_back((qrand()%10)/10);
        }
        res[1] = 0.99;
        return res;
    }
};

//TODO 完善对某个直播视频需要传输的全部相关信息
class VideoInfo{
public:
    QString Title;

};

class DataForwarding:QObject
{
    Q_OBJECT
public:
    DataForwarding( QObject * parent = 0);
    QTcpServer *Server;
    QTcpSocket *CurrentClient;
    QList<QTcpSocket*> clientList;
    friend QDataStream& operator>>(QDataStream &in,GeneralMessage& data);
    friend QDataStream& operator<<(QDataStream &out,GeneralMessage& data);
    TensorFlow tf;
private:
    QBlockPriorityQueue que;
    QMutex mutex;
    QPair<int,double> abnormityProb(QVector<double> res);//<事件ID,概率>
    QMap<int,int> CurrentVideoToEvent;
    QMap<int,QString> totalVideoList;//<视频ID,URL>

private slots:
    void newListen();
    void acceptConnection();
    void disConnected();
    void receiverMesg();
public slots:
    void sendCurrentResult();
signals:                     //<视频id,事件概率>     <视频id,视频地址>   <视频id,事件类型>
    void DataContainSignal(QMap<int,double>,QMap<int,QString>,QMap<int,int>);//<视频id,概率>
    void VideoListContainSignal(QMap<int,QString>);

};



#endif // DATAFORWARDING_H
