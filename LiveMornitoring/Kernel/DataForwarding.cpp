#include "DataForwarding.h"
#include"BasicFunction/app.h"
#include "BasicFunction/myhelper.h"

DataForwarding::DataForwarding(QObject *parent):
    QObject(parent)
{

    this->Server = new QTcpServer;
    this->CurrentClient = new QTcpSocket;
    newListen();

    connect(Server,SIGNAL(newConnection()),this,SLOT(acceptConnection()));
    //connect(Server,SIGNAL()
}

///得到异常事件的概率
QPair<int, double> DataForwarding::abnormityProb(QVector<double> res)
{
    return qMakePair(qrand()%10,0.97);
}

void DataForwarding::newListen()
{
    qDebug()<<"waiting for connecting";
    if(!Server->listen(QHostAddress::Any,7777)){
        qDebug()<<"ERR ";
    }
}

///新客户端接入
void DataForwarding::acceptConnection()
{
    qDebug()<<"SUCCESS"<<"  :"<<CurrentClient->peerAddress().toString();

    CurrentClient = Server->nextPendingConnection();
    this->clientList.push_back(CurrentClient);
    connect(CurrentClient,SIGNAL(disconnected()),this,SLOT(disConnected()));
    connect(CurrentClient,SIGNAL(readyRead()),this,SLOT(receiverMesg()));
}


void DataForwarding::disConnected()
{
    ///判断是哪个客户端断开了连接
    QTcpSocket* temp = (QTcpSocket*)sender();
    foreach (QTcpSocket* client, clientList) {

        qDebug()<<"Disconnect :"<<temp->peerAddress().toString();
        ///将断开连接的从列表中删除
       if(client->peerAddress()==temp->peerAddress()&&
               client->peerPort()==temp->peerPort()){
           clientList.removeOne(client);
       }
    }
}

void DataForwarding::receiverMesg()
{
    qDebug()<<"GetData";
    QTcpSocket* clientSender = (QTcpSocket*)sender();
    QByteArray arr;

    QDataStream * stream=new QDataStream(&arr,QIODevice::ReadOnly);/******重点******/
    stream->setVersion(QDataStream::Qt_5_7);
    arr = clientSender->readAll();
    GeneralMessage mesg;
    (*stream)>>mesg;

    int total_len = mesg.len;
    int len = 0;

    while(clientSender->waitForReadyRead(1000)){
        len += clientSender->bytesAvailable();
        arr.append((QByteArray)clientSender->readAll());
        if(total_len == len){
            break;
        }
    }
    qDebug()<<"Type"<<mesg.MESG_TYPE;
    qDebug()<<"UpdateVideoList:"<<mesg.VideoList[10];
    if(App::MSG_TYPE_FEATURE == mesg.MESG_TYPE){//接收特征

        QVector<double> res = tf.calc(mesg.feature[0]);//计算分类结果
        QPair<int,double> anomalousEvent = abnormityProb(res);
        double prob = anomalousEvent.second;
        int anomalousEventID = anomalousEvent.first;
        qDebug()<<"Feature recevied"+QTime::currentTime().toString("HH:mm:ss");
        ///记录当前视频分类对应的事件
        CurrentVideoToEvent[mesg.VideoID] = anomalousEventID;

        if(prob>App::Recall){
            mutex.lock();
            que.push(qMakePair(mesg.VideoID,prob));
            mutex.unlock();
        }

    }else if("2" == mesg.MESG_TYPE){//更新视频列表

        qDebug()<<"更新视频列表"<<mesg.VideoList[10];
        mutex.lock();
        totalVideoList = mesg.VideoList;
        mutex.unlock();
    }

}

//将当前需要人工审核的全部信息发送到播放端
void DataForwarding::sendCurrentResult()
{

    //QMap<int,double>,QMap<int,QString>,QMap<int,int>
    QMap<int,double> PROB_INFO;
    QMap<int,QString> URL_INFO;
    QMap<int,int>     EVENT_INFO;
    mutex.lock();
        for(int i = 0; i<16&&(!que.empty());i++){
            QPair<int,double> temp = que.pop();
            PROB_INFO[temp.first] = temp.second;
            URL_INFO[temp.first] = totalVideoList[temp.first];
            EVENT_INFO[temp.first] = CurrentVideoToEvent[temp.first];
        }
        emit DataContainSignal(PROB_INFO,URL_INFO,EVENT_INFO);
    mutex.unlock();
}


///序列化
QDataStream &operator>>(QDataStream &in, GeneralMessage &data)
{
    in>>data.feature>>data.MESG_TYPE>>data.SendTime>>data.VideoID>>data.VideoList>>data.result;
    return in;
}

///序列化
QDataStream &operator<<(QDataStream &out, GeneralMessage &data)
{

    out<<data.feature<<data.MESG_TYPE<<data.SendTime<<data.VideoID<<data.VideoList<<data.result;
    return out;
}
