#include "qblockpriorityqueue.h"

QBlockPriorityQueue::QBlockPriorityQueue()
{

}

void QBlockPriorityQueue::push(QPair<int, double> item)
{
    mutex.lock();
        pq.push(item);
    mutex.unlock();
}

QPair<int, double> QBlockPriorityQueue::pop()
{
    QPair<int, double> a;
    mutex.lock();
        a = pq.top();
        pq.pop();
    mutex.unlock();
    return a;

}

bool QBlockPriorityQueue::empty()
{
    return pq.empty();
}
