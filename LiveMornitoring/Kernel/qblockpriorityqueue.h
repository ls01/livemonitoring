#ifndef QBLOCKPRIORITYQUEUE_H
#define QBLOCKPRIORITYQUEUE_H
#include<queue>
#include <QPair>
#include <QMutex>
using std::priority_queue;

struct cmp{
    bool operator()(QPair<int,double> a,QPair<int,double> b){
        if(a.second>=b.second)
            return false;
           else
            return true;
    }
};

class QBlockPriorityQueue
{
public:
    QMutex mutex;
    priority_queue< QPair<int,double>, QVector< QPair<int,double> >,cmp > pq;
    QBlockPriorityQueue();
    void push(QPair<int,double> item);
    QPair<int,double> pop();
    bool empty();
};

#endif // QBLOCKPRIORITYQUEUE_H
