HEADERS += \    
    $$PWD/cpumemorylabel.h \
    $$PWD/frminputbox.h \
    $$PWD/frmmessagebox.h \
    $$PWD/datetimelcd.h \
    $$PWD/bottomwidget.h \
    $$PWD/devicesizetable.h \
    $$PWD/switchbutton.h\
    $$PWD/roundcircle.h\
    $$PWD/roundwidget.h\
    $$PWD/imageswitch.h\
	$$PWD/sliderbar.h\
	$$PWD/progressbarround.h\
	$$PWD/curvechart.h

SOURCES += \    
    $$PWD/cpumemorylabel.cpp \
    $$PWD/frminputbox.cpp \
    $$PWD/frmmessagebox.cpp \
    $$PWD/datetimelcd.cpp \
    $$PWD/bottomwidget.cpp \
    $$PWD/devicesizetable.cpp \
    $$PWD/switchbutton.cpp \
    $$PWD/roundcircle.cpp\
    $$PWD/roundwidget.cpp\
    $$PWD/imageswitch.cpp\
	$$PWD/sliderbar.cpp\
	$$PWD/progressbarround.cpp\
	$$PWD/curvechart.cpp
FORMS += \
    $$PWD/frminputbox.ui \
    $$PWD/frmmessagebox.ui
