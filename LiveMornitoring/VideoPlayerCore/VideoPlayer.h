#ifndef VIDEOPLAYER_H
#define VIDEOPLAYER_H
#include "videoplaycore.h"
#include "videostreamthread.h"
#include <QLabel>
#include <QObject>
#include <QMutex>


//对外播放接口，传入QWidget 与URL即可播放视频
class VideoPlayer:public QObject
{
    Q_OBJECT
public:
    //对外播放接口，传入QWidget 与URL即可播放视频
    VideoPlayer(QLabel* wind, QString url, QObject * parent = 0);
    void play();
    void stop();
    void PlayinTread();

private:
    QMutex mutex;
    QLabel* wind;
    VideoPlayerCore* vpc;
    VideoStreamThread *vth;
    QString url;
public slots:
    void paintFrameslot(const QImage &img);
    void sendFrame(const QImage img);
signals:
    void Frameinfo(const QImage &,QLabel*);
};

#endif // VIDEOPLAYER_H
