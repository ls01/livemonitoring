#ifndef PLAYERMULTITHREADENTRY_H
#define PLAYERMULTITHREADENTRY_H

#include <QObject>
#include "VideoPlayer.h"
/**
 * @brief The PlayerMultithreadEntry class多线程是播放服务的主要对外接口之一
 * @details PlayerMultithreadEntry可以完成将某个视频源（在线URL、本地视频或摄像头）投射到一个QWiget，将其作为播放容器。Url内容与QWiget限制条件需参考具体函数说明
 * @date
 */
class PlayerMultithreadEntry : public QThread
{
    Q_OBJECT
public:
    /**
     * @brief vp 播放器VideoPlayer的实例对象
     */
    VideoPlayer* vp;
    /**
     * @brief PlayerMultithreadEntry类的构造函数是本类对外提供服务的核心接口，参数wind与参数url将直接决定，视频本身以及呈现其的界面容器
     * @param wind 呈现视频所用的QWiget对象指针。这里需说明QWiget的子类对象指针同样可以被传入
     * @param url  这里推荐以标准Url格式在线视频源作为传入参数,需注意本接口暂不支持std::string，必须使用QString类型对象
     */
    PlayerMultithreadEntry(QLabel* wind, QString url);
    void run();
    QLabel* wind;
    QString url;
    QWidget* father;
public slots:

signals:
    void Frameinfo1(const QImage &,QLabel*);
};

#endif // PLAYERMULTITHREADENTRY_H
