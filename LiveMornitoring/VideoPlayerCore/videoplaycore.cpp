﻿#include "videoplaycore.h"

VideoPlayerCore::VideoPlayerCore(QObject *parent) : QObject(parent)
{
    //初始阶段 由于未设置Url 所以无法播放
    readyForPlay = false;
    stopFlag = false;
}

void VideoPlayerCore::play()
{
    if (readyForPlay == false)
            return;

        Mat tmpFrame;
        QImage qFrame;
        int q = 1;
        while (true)
        {

            if(stopFlag == true)
                break;
            q = (q + 1)%App::intervalFrames;//控制画面输出频率

            vcap >> tmpFrame;// 读一张画面

            tmpFrame.empty();
            //如果直播信号丢失
            if(tmpFrame.empty()){
                //输出错误信息
                qDebug()<<"No Signal :"<<url;
                vcap.release();
                emit Error(url);//发送播放中断信号
                break;

            }

            if(q==0){//输出画面
                qFrame = MatToQImage(tmpFrame);
                //qFrame.save(QString("url:%1 time:").arg(url).arg(QTime::currentTime::toString()));
                emit Getframe(qFrame);
                //qFrame.save();
            }


            waitKey(dely);
        }
}

void VideoPlayerCore::stop()
{
    mutex.lock();
    stopFlag = false;
    vcap.release();
    mutex.unlock();
}

bool VideoPlayerCore::openUrl(QString url)
{

    if (vcap.open(url.toStdString()) == false) {
        qDebug() << "Open Failed";
            return false;
    }
    this->url = url;
    readyForPlay = true;
    //获取视频FPS并计算延迟
    fps = vcap.get(CV_CAP_PROP_FPS);
    dely = (int)(1000.0/fps);
    qDebug()<<"Dely : "<<dely<<"FPS : "<<fps;
    //返回打开成功
    return true;

}

QImage VideoPlayerCore::MatToQImage(const Mat &mat)
{
    // 8-bits unsigned, NO. OF CHANNELS=1
        if (mat.type() == CV_8UC1)
        {
            // 配置颜色表(used to translate colour indexes to qRgb values)
            QVector<QRgb> colorTable;
            for (int i = 0; i < 256; i++)
                colorTable.push_back(qRgb(i, i, i));
            // Copy input Mat
            const uchar *qImageBuffer = (const uchar*)mat.data;
            // Create QImage with same dimensions as input Mat
            QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_Indexed8);
            img.setColorTable(colorTable);
            return img;
        }
        // 8-bits unsigned, NO. OF CHANNELS=3
        if (mat.type() == CV_8UC3)
        {
            // Copy input Mat
            const uchar *qImageBuffer = (const uchar*)mat.data;
            // Create QImage with same dimensions as input Mat
            QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
            return img.rgbSwapped();
        }
        else
        {
            qDebug() << "ERROR: Mat could not be converted to QImage.";
            return QImage();
        }
}
