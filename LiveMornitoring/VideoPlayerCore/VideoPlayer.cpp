#include "VideoPlayer.h"
#include<qDebug>
#include "FrameUI/frmvideo.h"

VideoPlayer::VideoPlayer(QLabel *wind, QString url/*void (MainWindow::*pfun2)(QImage img, QLabel *lab)*/,QObject *parent):
    QObject(parent)

{
    this->url = url;
    this->wind = wind;
    vpc = new VideoPlayerCore;
    vth = new VideoStreamThread;

}

void VideoPlayer::play()
{
    vpc->openUrl(url);
    connect(vpc,SIGNAL(Getframe(QImage)),this,SLOT(paintFrameslot(QImage)));
    vth->setVideoPlayerCore(vpc);
    vpc->play();
//    vth->start();

}

void VideoPlayer::stop()
{
    vpc->stop();
    vth->terminate();
}

void VideoPlayer::PlayinTread()
{
    vpc->openUrl(url);
    connect(vpc,SIGNAL(Getframe(QImage)),this,SLOT(sendFrame(QImage)));
    vpc->play();
//    vth->setVideoPlayerCore(vpc);
//    vth->start();
}

void VideoPlayer::paintFrameslot(const QImage &img)
{

    mutex.lock();

    int tempWidth = wind->/*geometry().*/width();
    int tempHeight = wind->/*geometry().*/height();
    QPixmap pix;
    if(App::ScrnState == 0){
          pix = QPixmap::fromImage(img.scaled(207, 141,Qt::IgnoreAspectRatio,App::HQVideo));
        //qDebug()<<"S";
        }
    else if(App::ScrnState == 1){
         pix = QPixmap::fromImage(img.scaled(474, 264,Qt::IgnoreAspectRatio,App::HQVideo));
        //qDebug()<<"M";
        //pix = QPixmap::fromImage(img.scaled(100, 70));
        }
    else{
        if(wind ==  frmVideo::currentClickedLabel)
            pix = QPixmap::fromImage(img.scaled(1910, 1070,Qt::IgnoreAspectRatio,App::HQVideo));
        else
            pix = QPixmap::fromImage(img.scaled(207, 141,Qt::IgnoreAspectRatio,App::HQVideo));


    }


    wind->setPixmap(pix);
    mutex.unlock();
    //w->paintFrames(img,wind);
}

void VideoPlayer::sendFrame(const QImage img)
{
    emit Frameinfo(img,wind);
}
