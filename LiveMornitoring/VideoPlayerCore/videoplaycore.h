﻿#ifndef VIDEOPLAYCORE_H
#define VIDEOPLAYCORE_H
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <QObject>
#include <QImage>
#include <Qdebug>
#include <QMutex>
#include "BasicFunction/app.h"
using namespace cv;
class VideoPlayerCore : public QObject
{
    Q_OBJECT
public:
    explicit VideoPlayerCore(QObject *parent = 0);
    void play();
    void stop();
    bool openUrl(QString url);
private:
    QMutex mutex;
    cv::VideoCapture vcap;
    QString url;
    bool readyForPlay;
    QImage MatToQImage(const Mat& mat);//将CV图片Mat转换为Qt的QImage
    int dely;
    bool stopFlag;
    double fps;
signals:
    void Getframe(const QImage&);
    void Error(QString url);//因为url是私有成员，所以这里发送包含url的错误信号
public slots:
};

#endif // VIDEOPLAYCORE_H
