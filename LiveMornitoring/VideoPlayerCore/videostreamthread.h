﻿#ifndef VIDEOSTREAMTHREAD_H
#define VIDEOSTREAMTHREAD_H

#include <QObject>
#include <QThread>
#include "videoplaycore.h"

class VideoStreamThread : public QThread
{
    Q_OBJECT
public:
    explicit VideoStreamThread(QObject *parent = 0);
    void run();
    void setVideoPlayerCore(VideoPlayerCore* vpc);
private:
    VideoPlayerCore* vpc;

};

#endif // VIDEOSTREAMTHREAD_H
